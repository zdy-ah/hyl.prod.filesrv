package com.entity;
/**
 * 这个类暂时不用
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


import hyl.core.MyFun;
import hyl.core.conf.MyConst;
import hyl.core.io.MyFile;

public class UserFileComp {


	// 0:其他文件, 1:数据文件,2:缓存文件 a:身份证图片 ,b:银行卡图片 ,c:人像图片,d:产品图,e:网页图片
	public static final char scope_file_其他 = '9';
	public static final char scope_file_数据 = '0';
	public static final char scope_file_缓存 = '1';
	public static final char scope_file_聊天 = '2';
	public static final char scope_img_其他 = 'z';
	public static final char scope_img_身份证 = 'a';
	public static final char scope_img_银行卡 = 'b';
	public static final char scope_img_人像 = 'c';
	public static final char scope_img_产品 = 'd';
	public static final char scope_img_文档 = 'e';
	public static final char scope_img_聊天 = 'f';
	
	
	// 接收用户文件
	// 返回相对路径
	public static String upload(Integer usid, String scope, Iterator<Part> parts) throws Exception {
		String tagdir = usid + File.separator + scope;
		String tagpath = FileComp.getUploadPath() + File.separator + tagdir;
		
		File newfile=null;
		String fileid =null;
		MyFile.openDir(tagpath);
		while (parts.hasNext()) {// 遍历写文件
			Part part = parts.next();
			String filetype = part.getContentType();
			Long srcsize=part.getSize();
			String oldfilename = FileComp.extractFilename(part);
			if (MyFun.isEmpty(oldfilename))
				continue;
			String fname = MyFun.getUUID()+oldfilename.substring(oldfilename.lastIndexOf("."));
			// 保存新文件		
			newfile = MyFile.saveFile(1, part.getInputStream(), tagpath,fname);
			fileid  = newfile.getName();
			Long newSize= newfile.length();			
			SqlOfFile.insfile(fileid, usid, null, oldfilename, null,newSize, filetype, scope);
		}
		return "f/download/"+scope+'/'+fileid;
	}

	public static boolean download(Integer usid, String scope, String fileid, HttpServletResponse res)
			throws FileNotFoundException, IOException {
		String tagdir = usid + File.separator + scope;
		String tagpath = FileComp.getUploadPath() + File.separator + tagdir;
		String downLoadPath = tagpath + File.separator + fileid;
		
		//Map<String, Object> fmap = SqlOfFile.getFile(fileid,"2");
		//if (fmap.isEmpty())
		//	return false;
		File file = new File(downLoadPath);
		if (!file.exists()) {
			return false;
		} else {
			// 获取文件的长度
			Long fileLength = file.length();
			// 设置文件输出类型
			// 清空response
			res.reset();
			// 设置response的Header
			res.addHeader("Content-Disposition", "attachment;filename=" + fileid);
			// 设置输出长度
			res.addHeader("Content-Length", fileLength.toString());
			// response.setContentType("multipart/form-data");
			res.setContentType("application/octet-stream");
			MyFile.copyFile(new FileInputStream(file), res.getOutputStream());
			return true;
		}
	}

	/* 获取用户所有有文件*/
	public static List<Map<String, Object>> getSupFiles(Integer usid, String scope, Integer max) {
		if (max == null || usid == null || scope == null)
			return null;
		return SqlOfFile.getOldFiles(usid, scope, max);
		

	}

	public static void delFiles(Integer usid, String scope, String[] ids) {
		List<Object[]> listids = new ArrayList<>();
		listids.add(ids);	
		String tagdir = usid + File.separator + scope;
		String tagpath =FileComp. getUploadPath() + File.separator + tagdir;
		for (String id : ids) {
			String file = tagpath + File.separator + id;
			MyFile.deleteFile(file);
		}
		SqlOfFile.delfiles(listids);
	}
	public static void delFile(String fileid) throws Exception {	
		Map<String, Object> fmap=SqlOfFile.getFile(fileid,null);
		SqlOfFile.delfile(fileid);
		String tagdir = ( int)fmap.get("uid")  + File.separator + (char)fmap.get("scope");
		String tagpath = FileComp.getUploadPath() + File.separator + tagdir;
		String file = tagpath + File.separator + fileid;
		MyFile.deleteFile(file);
	}
	public static void delFile(Integer usid, Character scope, String fileid) {
		SqlOfFile.delfile(fileid);
		String tagdir = usid + File.separator + scope;
		String tagpath = FileComp.getUploadPath() + File.separator + tagdir;
		String file = tagpath + File.separator + fileid;
		MyFile.deleteFile(file);
	}

	public static Map<String, Object> getFileById(String fileid) {
		return SqlOfFile.getFile(fileid,"2");
	}
}
