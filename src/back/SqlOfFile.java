package com.entity;
/**
 * 这个类暂时不用
 */
import java.util.List;
import java.util.Map;

import com.servlet.My;

import hyl.core.MyFun;
import hyl.core.db.MyDB;

public class SqlOfFile {

	// 查找同名文件
	public static List<Map<String, Object>> getSameFiles(Integer 用户编号, String 适用场景, String 源文件名) {
		String sql = "select id,ctime,srcpath,srcfile,state from user_files where uid=?  AND scope=? and srcfile=?  order by ctime ";
		Object[] params = { 用户编号, 适用场景, 源文件名 };
		MyDB db = My.getDB1();
		return db.queryList(sql, params);
	}

	// 插入文件
	public static Object insfile(String fileid, Integer 用户编号, String 源路径, String 源文件名, String 目标路径, Long 文件大小,
			String 文件类型, String 适用场景) {
		String sql = "INSERT INTO user_files(ctime,id,uid,srcpath,srcfile,tagpath,fsize,ftype,scope) "
				+ "	VALUES(NOW(),?,?,?,?,?,?,?,?)";
		Object[] params = { fileid, 用户编号, 源路径, 源文件名, 目标路径, 文件大小, 文件类型, 适用场景 };
		StringBuilder sb=new StringBuilder();
		sb.append("INSERT INTO user_files(ctime,id,uid,srcfile,fsize,ftype,scope)VALUES(NOW(),");
		sb.append("'").append(fileid).append("',");
		sb.append(用户编号);
		sb.append(",'").append(源文件名).append("'");
		sb.append(",").append(文件大小);
		sb.append(",'").append(文件类型).append("'");
		sb.append(",'").append(适用场景).append("')");
	
		MyDB db = My.getDB1();
		return db.executeUpdate(sql, params);
		//System.out.println(sb.toString());
		//return db.executeUpdate(sb.toString());
	}

	// 得到多余的文件
	public static List<Map<String, Object>> getOldFiles(Integer 用户编号, String 适用场景, Integer max) { // 最多保留10个备份
		String sql = "call p_getSurplusFile(?,?,?)";
		Object[] params = { 用户编号, 适用场景, max };
		MyDB db = My.getDB1();
		return db.queryList(sql, params);
	}

	// 得到文件
	public static Map<String, Object> getFile(String id, String state) { // 最多保留10个备份
		/// _idkey,_syschgdate,_datekey,uid,srcfile,tarpath,tarfile,maxage,scope,
		if (MyFun.isEmpty(id))
			return null;
		String sql = "SELECT id,uid, scope,ctime,tagpath,srcpath,srcfile,state FROM user_files ";
		MyDB db = My.getDB1();
		if (state == null) {
			Object[] params = {  id };
			sql = sql + "WHERE id=? ";
			return db.queryMap(sql, params);
		} else {
			Object[] params = { state, id };
			sql = sql + "WHERE state=? and  id=? ";
			return db.queryMap(sql, params);
		}
	}

	// 得到所有文件
	public static List<Map<String, Object>> getFiles(Integer 用户编号, String 适用场景, String state) { // 最多保留10个备份
		// _idkey,_syschgdate,_datekey,uid,srcfile,tarpath,tarfile,maxage,scope,
		if (用户编号==null||适用场景==null)
			return null;
		String sql = "select id,uid, scope,ctime,tagpath,srcpath,srcfile,state from user_files ";
		MyDB db = My.getDB1();
		if (state == null) {
			Object[] params = { 用户编号, 适用场景 };
			sql = sql + "where uid=?  AND scope=? order by ctime desc";
			return db.queryList(sql, params);
		} else {
			Object[] params = { 用户编号, 适用场景, state};
			sql = sql + "where uid=?  AND scope=? and state=?  order by ctime desc";
			return db.queryList(sql, params);
		}
	}

	// 批量删除文件
	public static void delfiles(List<Object[]> ids) {
		MyDB db = My.getDB1();
		String sql = "delete from user_files where id=? ";
		db.batch(sql, ids);

	}

	// 删除单个文件
	public static void delfile(String ids) {
		MyDB db = My.getDB1();
		String sql = "delete from user_files where id=? ";
		db.executeUpdate(sql, new Object[] { ids });

	}

	// 修改文件权限
	public static void updateState(String id, String state) {
		if (MyFun.isEmpty(id)||state==null)
			return ;
		MyDB db = My.getDB1();
		String sql = "update user_files set state=? where id=? ";
		db.executeUpdate(sql, new Object[] { state, id });

	}
}
