package com.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.entity.FileComp;

@WebServlet("/down/df/*")
public class DownloadDFSControl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");

		/*
		 * 
		 * System.out.println(request.getContextPath());// /fs
		 * System.out.println(request.getLocalAddr());
		 * System.out.println(request.getServletPath());// down
		 * System.out.println(request.getRemoteAddr());
		 */

		StringBuffer path = req.getRequestURL();

		int index = path.indexOf("/down/df/") + 8;
		String fpath = path.substring(index);

		FileComp.download(fpath, 1, req, resp);

	}

}
