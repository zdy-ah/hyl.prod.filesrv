package com.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/a")
public class testControl extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getBasePath() {
	    WebServlet webServlet = this.getClass().getAnnotation(WebServlet.class);
	    String path = null;
	    
	    if (webServlet.value().length > 0 ) {
	        path = webServlet.value()[0];
	    }
	    if(webServlet.urlPatterns().length>0){
	        path = webServlet.urlPatterns()[0];
	    }
	    if(null == path) {
	        return null;
	    }
	    if (!path.endsWith("/*")) {
	        return "";
	    } else {
	        return path.substring(0, path.length() - 2);
	    }

	}
	public void printPageExt() {
	    WebServlet an = this.getClass().getAnnotation(WebServlet.class);
	    String[] uplist = an.urlPatterns();
	    if (uplist==null) return ;
	    for (String s :uplist )
	    	System.out.println(s);
	}
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		System.out.println("kk"+req.getRequestURL());
		System.out.println("-1-");
		printPageExt();
		System.out.println("-2-");
		System.out.println(getBasePath());
		System.out.println("-3-");
	}
}
