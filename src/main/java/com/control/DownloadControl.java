package com.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.entity.FileComp;

@WebServlet("/down/f/*")
public class DownloadControl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public String getBasePath() {
		WebServlet webServlet = this.getClass().getAnnotation(WebServlet.class);
		String path = null;

		if (webServlet.value().length > 0) {
			path = webServlet.value()[0];
		}
		if (webServlet.urlPatterns().length > 0) {
			path = webServlet.urlPatterns()[0];
		}
		if (null == path) {
			return null;
		}
		if (!path.endsWith("/*")) {
			return "";
		} else {
			return path.substring(0, path.length() - 2);
		}

	}

	public String getPageExt() {
		WebServlet an = this.getClass().getAnnotation(WebServlet.class);
		String[] uplist = an.urlPatterns();
		return uplist[0].substring(2);
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");

		StringBuffer path = req.getRequestURL();
		int index = path.indexOf("/down/f/") + 7;
		String fpath = path.substring(index);
		FileComp.download(fpath, 0, req, resp);

	}

}
