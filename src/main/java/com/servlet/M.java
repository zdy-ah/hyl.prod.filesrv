package com.servlet;

import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import hyl.core.MyFun;

/** 系统级别的配置 */
public final class M {
	//////////////////// 公共字典////////////////////////////////
	public static final int CODE_非法尝试 = 401;
	public static final int CODE_未登录 = 102;
	public static final int CODE_已登录 = 1;
	public static final String[] MSG_验证通过 = { "Verified", "验证通过" };
	public static final String[] MSG_频繁 = { "Your operation is too frequent!", "您的操作太频繁了!" };
	public static final String[] MSG_注册成功 = { "Your operation is too frequent!", "注册成功!" };
	public static final String[] MSG_注册失败 = { "Registration failed. Please try again.!", "注册失败,请重试!" };
	public static final String[] MSG_手机验证码错误 = { "SMS verification code is incorrect.", "手机短信验证码不正确" };
	public static final String[] MSG_验证码错误 = { "Verification code is incorrect", "验证码不正确" };
	public static final String[] MSG_登录名或密码错误 = { "Login name or password is incorrect", "登录名或密码错误" };
	public static final String[] MSG_登录名或密码不能为空 = { "Login name or password cannot be empty", "登录名或密码不能为空" };
	public static final String[] MSG_登录成功 = { "Successfully login", "登录成功" };
	public static final String[] MSG_不能为空 = { "Required fields can not be empty.", "必填项不能为空" };
	public static final String[] MSG_请求短信验证码 = { "SMS verification code first", "请先请求短信验证码" };
	public static final String[] MSG_请求错误 = { "The requested url is incorrect, please reconfirm", "请求的url错误,请重新确认" };
	public static final String[] MSG_异常登录 = { "Login exception!", "请注意,登录异常!" };
	public static final String[] SMS_验证短信 = {
			"Your registration verification code is [%s]. If you are not yourself,please do not operate it.",
			"您的注册验证码是【%s】，如非本人请勿操作。" };
	public static final String[] SMS_注册通过短信 = {
			"Dear [%s], your account has been registered with an initial password of [%s].",
			"尊敬的【%s】，您的账户已注册，初始密码为【%s】" };
	private static final String URL_login_front = "/admin/login1.jsp";
	private static final String URL_main_front = "/admin/main1.jsp";
	private static final String URL_login_admin = "/admin/login2.jsp";
	private static final String URL_main_admin = "/admin/main2.jsp";
	public static final String[] MSG_阻拦 = {
			"Your operation is abnormal and the system automatically intercept your behavior, please try again in 30 minutes!",
			"您的操作行为异常,系统自动拦截,请在30分钟后再操作!" };
	public static final int ROLE_管理员 = 1;
	public static final int ROLE_会员 = 2;
	public static final int W左区 = 0;
	public static final int W右区 = 1;
	public static final int U管理员 = 1;
	// 兄弟站点 白名单 不在兄弟站点内的是 黑名单,禁止直连
	private static final Set<String> URL_codes = new HashSet<>();
	static {
		URL_codes.add("");
	}

	public static boolean isBrother(String idcode) {
		if (MyFun.isEmpty(idcode))
			return false;
		return URL_codes.contains(idcode);
	}

	//////////////////////// 每套系统有关的字典定义///////////////////////
	public static final String getUrlLogin(HttpServletRequest request) {
		String url = request.getHeader("referer");
		if (url.indexOf("/jsp/") > 0)
			return URL_login_front;
		else
			return URL_login_admin;
	}

	public static final String getUrlAdmin(HttpServletRequest request) {
		String url = request.getHeader("referer");
		if (url.indexOf("/jsp/") > 0)
			return URL_main_front;
		else
			return URL_main_admin;
	}

	public static final char F是 = 'y';
	public static final char F否 = 'n';

	public static final String get是否(char sf) {
		if (sf == F是)
			return "是";
		else
			return "否";
	}

	public static final String get是与否(String sf) {
		// System.out.println(sf);
		if (sf.equals("y"))
			return "是";
		else
			return "否";
	}
}