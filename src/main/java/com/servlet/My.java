package com.servlet;
import javax.servlet.http.HttpServletRequest;

import com.entity.FileComp;

import hyl.core.AIni;
public class My extends AIni {
	protected static My my;
	private My() {
	}
	public static My getInstance() {
		if (my == null) {
			my = new My();
		}
		TaskMission();
		return my;
	}
	static String baseurl;
	static String basePath;
	public static String getBasePath(HttpServletRequest request) {
		if (baseurl == null) {
			baseurl = "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() ;
			basePath = request.getScheme() + baseurl;
		}
		return basePath;
	}
	public static void reset() {
		my = new My();
		TaskMission();
	}
	/**
	 * 适用于直接访问的jsp页面
	 */
	public static void TaskMission() {
		/*
		 * MyTimer.getInstance("日结算").dayStart( new IDo<Calendar>() {
		 * 
		 * @Override public void run(Calendar obj) throws Exception { Calendar ca =
		 * Calendar.getInstance(); ca.setTime(new Date()); ca.add(Calendar.DATE, -1);
		 * Date lastMonth = ca.getTime(); SimpleDateFormat sdf = new
		 * SimpleDateFormat("yyyyMMdd"); int a =
		 * Integer.parseInt(sdf.format(lastMonth)); SqlOfReward.cnt_日结算(a); } },
		 * MyFun.str2Datetime("2019-4-23 00:01:00"));
		 */
	}
	/**
	 * 适用于直接访问的jsp页面 2019.3.5 sx
	 */
	public static void exit() {
		closeAllThreads();
		FileComp.exit();
	}
	public static void onErrClose() {
		onExit = new Runnable() {
			@Override
			public void run() {
				Log.info("异常关闭触发事件");
				exit();
			}
		};
	}
}
