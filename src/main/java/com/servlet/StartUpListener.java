package com.servlet;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
/**
 * Application Lifecycle Listener implementation class StartUp
 */
@WebListener
public class StartUpListener implements ServletContextListener {
	/**
	 * Default constructor.
	 */
	public StartUpListener() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	
		My.Log.info("网站关闭了..........");
		My.exit();
	}
	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		// System.out.println("StartUpListener-0:初始化My");

		//My.Log.info("网站启动了..............");
		//My.getInstance();
		// 设置文件默认路径
		// System.out.println("StartUpListener-1:设置文件默认路径");
	}
}
