package com.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class Anquan
 */
@WebFilter("/MyFilter")
public class MyFilter implements Filter {
	/**
	 * Default constructor.
	 */
	public MyFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// response.setCharacterEncoding("UTF-8");
		// request.setCharacterEncoding("UTF-8");
		// response.setContentType("application/json; charset=utf-8");
		// System.out.println("lll的"+((HttpServletRequest) request).getRequestURI());
		((HttpServletRequest) request).getSession(false);
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Origin", "*");
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Headers", "Content-Type,token"); // 加入支持自定义的header
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Methods", "PUT,POST,GET");
		// ((HttpServletResponse) response).setHeader("Content-Type",
		// "application/json;charset=utf-8");

		chain.doFilter(request, response);
	}

	// ((HttpServletResponse ) response).setHeader("X-Powered-By"," 3.2.1");
	// response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,token");
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
}
